//
//  StringTexterTests.swift
//  StringTexterTests
//
//  Created by Michael Graziano on 9/8/18.
//  Copyright © 2018 Michael Graziano. All rights reserved.
//

import XCTest
@testable import StringTexter

class StringTexterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    /// A Case where the entire message will fit in the given SMS length
    func testSingleMessage() {
        let testString = "SMS messages are really short"
        XCTAssertEqual(1, StringTexter().solution(testString, testString.count))
    }
    
    /// A case where the message must be broken into pieces to fit
    func testMultipleMessageSuccess() {
        let testString = "SMS messages are really short"
        XCTAssertEqual(3, StringTexter().solution(testString, 12))
    }
    
    /// A case where the message is one word, but longer than the allowable length
    func testTooLongSingleWord() {
        let testString = "Testing123"
        XCTAssertEqual(-1, StringTexter().solution(testString, testString.count - 1))
    }
    
    /// A case where a single word is too long for a text
    func testMultiwordTooLong() {
        let testString = "This is a fantastic test"
        XCTAssertEqual(-1, StringTexter().solution(testString, "fantastic".count - 1))
    }
    
    /// Test a case where one word meets the limit exactly
    func testExactLengthSMS() {
        XCTAssert(StringTexter().solution("aaaaaaaaaa bbbbb bbbbb cccccccccc", 10) == 4)
    }
    
    
    /// Test case where each word is under the limit, but the space puts it at the limit
    func testSingleChars() {
        XCTAssert(StringTexter().solution("A b C", 2) == 3)
    }

    /// Test case with few words, but the limit is mid-second word
    func testTwoWordsUnderLimit() {
        XCTAssert(StringTexter().solution("idmfuez ifkejdiskf", 17) == 2)
    }
    
    /// Test case where a word other than the first is the limit
    func testWordsUpToLimit() {
        XCTAssert(StringTexter().solution("K Sd", 2) == 2)
    }

    /// Test case with exactly 500 characters
    func testExactlyOverallCharacterLimit() {
        let loremIpsum = "alias consequatur aut perferendis sit voluptatem accusantium doloremque aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt neque dolorem ipsum quia dolor sit amet consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem ut enim ad minima veniam maiores doloribus xx"
            XCTAssert(StringTexter().solution(loremIpsum, 12) == 52)
    }

    /// Test Case with a really long string
    func testReallyLongString() {
        let loremIpsumLarge = "alias consequatur aut perferendis sit voluptatem accusantium doloremque aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt neque dolorem ipsum quia dolor sit amet consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem ut enim ad minima veniam quis nostrum exercitationem ullam corporis nemo enim ipsam voluptatem quia voluptas sit suscipit laboriosam nisi ut aliquid ex ea commodi consequatur quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae et iusto odio dignissimos ducimus qui blanditiis praesentium laudantium totam rem voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident sed ut perspiciatis unde omnis iste natus error similique sunt in culpa qui officia deserunt mollitia animi id est laborum et dolorum fuga et harum quidem rerum facilis est et expedita distinctio nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo porro quisquam est qui minus id quod maxime placeat facere possimus omnis voluptas assumenda est omnis dolor repellendus temporibus autem quibusdam et aut consequatur vel illum qui dolorem eum fugiat quo voluptas nulla pariatur at vero eos et accusamus officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae itaque earum rerum hic tenetur a sapiente delectus ut aut reiciendis voluptatibus maiores doloribus asperiores repellat"
        XCTAssert(StringTexter().solution(loremIpsumLarge, 14) == 144)
    }
}
