//
//  StringTexter.swift
//  StringTexter
//
//  Created by Michael Graziano on 9/8/18.
//  Copyright © 2018 Michael Graziano. All rights reserved.
//

class StringTexter {

    /// Performs a check to see how many text messages a string would need to be split into to
    /// send, given an individual text character limit
    ///
    /// - Parameters:
    ///   - S: The string to be texted
    ///   - K: The character limit of a single text
    /// - Returns: The number of texts the string must be split into, or -1 if not possible
    public func solution(_ S: String, _ K: Int) -> Int {
        
        var wordsArray = S.components(separatedBy: " ")
        var smsArray: [String] = []
        
        /// Check to see if any words are above the individual string character limit, and that
        /// the input parameters follow the requirements ( <= 500 string length and char limit)
        if wordsArray.filter({ $0.count > K }).count != 0 { return -1 }
        
        /// While there are still words in the working array, calculate the next individual SMS and
        /// add it to the array
        while !wordsArray.isEmpty {
            smsArray.append(textSegment(&wordsArray, K))
        }
        
        return smsArray.count
    }
    
    /// Takes in an array of strings, removes words fitting within a given length, and
    /// returns the remaining words in an array
    ///
    /// - Parameters:
    ///   - wordsArray: An array of words to be broken into texts
    ///   - length: The length of a text
    /// - Returns: The SMS string that will be the next SMS sent
    private func textSegment(_ wordsArray: inout [String], _ length: Int) -> String {
        
        var count = 0
        var textSegmentArray: [String] = [] {
            didSet {
                /// Count represents the new total of letters plus re-inserted spaces
                count = textSegmentArray.reduce(0, { $0 + $1.count }) + textSegmentArray.count
            }
        }
        
        /// Iterate through the array of words, adding the word at index 0 to the SMS array
        /// if it will not put the character count over the limit
        while count < length {
            if let nextWord = wordsArray.first,
                count + nextWord.count <= length {
                textSegmentArray.append(wordsArray.removeFirst())
            } else { break }
        }
        
        /// Return the string that will be sent as a text
        return textSegmentArray.joined(separator: " ")
    }
}
